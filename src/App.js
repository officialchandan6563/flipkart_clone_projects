import React from 'react';
import Header from './Components/Header/Header';
import Header_2 from './Components/Header2/header_2';
import Banner from './Components/Banner/banner';
import Index from './Components/Product/Index';
function App() {
  return (
    <div style={{backgroundColor:'rgb(236, 243, 243)', width:'100%',height:'auto'}}>
    <Header/>
    <Header_2/>
    <Banner/>
    <Index/>
    </div>
  );
}

export default App;
