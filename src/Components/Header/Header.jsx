import React from 'react';
import './headerStyle.css'
import Main_logo from './../../Assets/images/flipkart logo.svg'
import Search_bar from './../../Assets/images/search-icon.svg'
import Profile_logo from './../../Assets/images/profile-logo.svg'
import addCart_logo from './../../Assets/images/header_cart.svg'
import Seller_icon from './../../Assets/images/Store.svg'
import threeDots from './../../Assets/images/header_3verticalDots.svg'

function Header() {
  return (
    <div className='Header_main_block'>
      <div className="Header_sub_block">
         <div className="Header_sub_block_card_1">
            <img src={Main_logo} alt="img" />
        </div>
        <div className="Header_sub_block_card_2">
            <i class="fa fa-search"></i>
            <input type="text" placeholder='Search for Product, Brands and more' />
        </div>
        <div className="Header_sub_block_card_3">
            <img src={Profile_logo} alt="img" />
            <p>Login</p>
        </div>
        <div className="Header_sub_block_card_4">
            <img src={addCart_logo} alt="img" />
            <p>Cart</p>
        </div>
        <div className="Header_sub_block_card_5">
            <img src={Seller_icon} alt="img" />
            <p>Become a Seller</p>
        </div>
        <div className="Header_sub_block_card_6">
            <img src={threeDots} alt="img" />
        </div>
      </div> 
    </div>
  )
}

export default Header
