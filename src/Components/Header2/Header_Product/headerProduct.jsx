import React from 'react'
import './headerProductStyle.css'

function headerProduct() {
  return (
    <div className='header_product_parents_block'>
      <div className="header_product_block_card1">
        <p>Mens Top Wear</p>
        <p>Mens Bottom Wear</p>
        <p>Women Ethnic</p>
        <p>Women Western</p>
        <p>Men Footwear</p>
        <p>Women Foowear</p>
        <p>Watches and Accessories</p>
        <p>Bags, Suitcase and Luggages</p>
        <p>Kids</p>
        <p>Essentials</p>
        <p>Winter</p>
      </div>
      <div className="header_product_block_card2">
        <h5>More in Men's Top Wear</h5>
        <p>All</p>
        <p>Men's T-Shirts</p>
        <p>Men's Casual Shirts</p>
        <p>Men's Formal Shirts</p>
        <p>Men's Kurtas</p>
        <p>Men's Ethnic sets</p>
        <p>Men's Blazers</p>
        <p>Men's Raincoats</p>
        <p>Men's Raincoat</p>
        <p>Men's Windcheaters</p>
        <p>Men's Suits</p>
        <p>Men's Fabric</p>
      </div>

    </div>
  )
}

export default headerProduct
