import React, { useState } from "react";
import "./header2Style.css";
import Surf from "./../../Assets/images/header2 img/surfaxcel.png";
import Mobile from "./../../Assets/images/header2 img/mobile.png";
import Fashion from "./../../Assets/images/header2 img/fashion.png";
import Electronics from "./../../Assets/images/header2 img/electronics.png";
import Furniture from "./../../Assets/images/header2 img/furniture.jpg";
import Appliences from "./../../Assets/images/header2 img/applience.jpg";
import Travel from "./../../Assets/images/header2 img/treavels.png";
import Beauty_Toys from "./../../Assets/images/header2 img/beauty&toy.png";
import Two_wheelers from "./../../Assets/images/header2 img/two whillers.png";

import Header_Product from "./Header_Product/headerProduct";
function Header2() {
  const [isHovered, setIsHovered] = useState(false);

  return (
    <div className="header2_main_block">
      <div className="header2_sub_block">
        <div className="header2_sub_block_card">
          <img src={Surf} alt="img" />
          <p>Grocery</p>
        </div>
        <div className="header2_sub_block_card">
          <img src={Mobile} alt="img" />
          <p>Mobiles</p>
        </div>
        <div
          className="header2_sub_block_card"
          onMouseEnter={() => setIsHovered(true)}
          onMouseLeave={() => setIsHovered(false)}
          style={{ cursor: "pointer" }} // Change cursor to pointer when hovered
        >
          <img src={Fashion} alt="img" />
          <p
            style={{
              color: isHovered ? "blue" : "black", // Change text color based on isHovered state
            }}
          >
              Fashion
              <span>
                <i
                  className="fa fa-angle-down"
                  style={{
                    transform: isHovered ? "rotate(180deg)" : "rotate(0deg)",
                    transition: "transform 0.2s ease-in-out",
                  }}
                ></i>
              </span>
          </p>
          {isHovered && <Header_Product />}{" "}
        </div>
        <div className="header2_sub_block_card">
          <img src={Electronics} alt="img" />
          <p>
            Electronics
            <span>
              <i className="fa fa-angle-down"></i>
            </span>
          </p>
        </div>
        <div className="header2_sub_block_card">
          <img src={Furniture} alt="img" />
          {/* <p
      style={{ color: isHovered ? 'blue' : 'black' }}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      Home & Furniture
      <span style={{ transform: isHovered ? 'rotate(180deg)' : 'rotate(0deg)' }}>
        <i className="fa fa-angle-down"></i>
      </span>
    </p> */}
          <p>
            Home & Furniture
            <span>
              <i className="fa fa-angle-down"></i>
            </span>
          </p>
        </div>
        <div className="header2_sub_block_card">
          <img src={Appliences} alt="img" />
          <p>Appliances</p>
        </div>
        <div className="header2_sub_block_card">
          <img src={Travel} alt="img" />
          <p>Travel</p>
        </div>
        <div className="header2_sub_block_card">
          <img src={Beauty_Toys} alt="img" />
          <p>
            Beauty, Toys & More
            <span>
              <i className="fa fa-angle-down"></i>
            </span>
          </p>
        </div>
        <div className="header2_sub_block_card">
          <img src={Two_wheelers} alt="img" />
          <p>
            2-Wheelers
            <span>
              <i className="fa fa-angle-down"></i>
            </span>
          </p>
        </div>
      </div>
    </div>
  );
}

export default Header2;
