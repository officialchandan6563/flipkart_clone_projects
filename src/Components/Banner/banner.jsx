import React, { useState, useEffect } from 'react';
import './bannerStyle.css';
import Banner1 from './../../Assets/images/banner.jpg';
import Banner2 from './../../Assets/images/header2 img/home_banner.webp';

function Banner() {
  const imgUrls = [Banner1, Banner2, Banner1, Banner2];
  const [currentImageIndex, setCurrentImageIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentImageIndex((prevIndex) => (prevIndex + 1) % imgUrls.length);
    }, 2000); // Adjust slide duration as needed (milliseconds)

    return () => clearInterval(interval);
  }, [imgUrls.length]);

  const slideLeft = () => {
    const newIndex = (currentImageIndex - 1 + imgUrls.length) % imgUrls.length;
    setCurrentImageIndex(newIndex);
  };

  const slideRight = () => {
    const newIndex = (currentImageIndex + 1) % imgUrls.length;
    setCurrentImageIndex(newIndex);
  };
console.log("currentImageIndex",currentImageIndex)
  return (
    <div className='banner_main_block' style={{ backgroundImage: `url(${imgUrls[currentImageIndex]})` }}>
        <i className="fa fa-angle-left" onClick={slideLeft}></i>
        <i className="fa fa-angle-right" onClick={slideRight}></i>
    </div>
  );
}

export default Banner;