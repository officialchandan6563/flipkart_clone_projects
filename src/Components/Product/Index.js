import React from "react";
import "./productStyle.css";
import BeautyProduct from "./BeautyProduct";
import ElectronicProduct from "./ElectronicProduct";
import LikeProduct from "./LikeProduct";
import ExploreProduct from "./ExploreProduct";
import EssentialProduct from "./EssentialProduct";
import RecomendedProduct from "./RecomendedProduct";
function product() {
  return (
    <>
      {/* ElectronicProduct */}
      <ElectronicProduct />

      {/* Beauty component call */}
      <BeautyProduct />

      {/* --------------You May Like....--------------------------- */}
      <LikeProduct />

      {/* -------------More to Explore----------------------------- */}
      <ExploreProduct />

      {/* ---------------Home Kitchen & Essentials------------------------------- */}
      <EssentialProduct />

      {/* --------------Recomended Items------------------------- */}
      <RecomendedProduct />
    </>
  );
}

export default product;
