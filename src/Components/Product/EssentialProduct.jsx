import React, { useEffect, useState } from 'react'
import Camere from "./../../Assets/images/Product img/camera.jpg";
import Trimmer from "./../../Assets/images/Product img/trimmer.jpg";

function EssentialProduct() {
  const [data, setData] = useState([]);

  const ElectronicData = [
    {
      "product_img": Camere,
      "product_name": "Top Mirrorless Camera",
      "product_heading": "Shop Now!"
    },
    {
      "product_img": Trimmer,
      "product_name": "Best of Shavers",
      "product_heading": "From $29"
    },
    {
      "product_img": Camere,
      "product_name": "Top Mirrorless Camera",
      "product_heading": "Shop Now!"
    },
    {
      "product_img": Trimmer,
      "product_name": "Best of Shavers",
      "product_heading": "From $29"
    },
    {
      "product_img": Camere,
      "product_name": "Top Mirrorless Camera",
      "product_heading": "Shop Now!"
    },
    {
      "product_img": Trimmer,
      "product_name": "Best of Shavers",
      "product_heading": "From $29"
    },
    {
      "product_img": Camere,
      "product_name": "Top Mirrorless Camera",
      "product_heading": "Shop Now!"
    },
    {
      "product_img": Trimmer,
      "product_name": "Best of Shavers",
      "product_heading": "From $29"
    }
  ];

  useEffect(() => {
    // api call  function
    // {
      // response
    //   setData(response);
    // }
    setData(ElectronicData);
  }, []);


  return (
    <>
      <div className="electronic_main_block">
        <div className="electronic_sub_block">
          <div className="elcetronic_sub_block_content">
            <h2>Best of Electronic</h2>
            <i className="fa fa-angle-right"></i>
          </div>
          <div className="ecectronic_sub_block_card">
            {data.map((item, index) => (
              <div className="electronic_sub_bloc_card_content" key={index}>
                <img src={item.product_img} alt="img" />
                <div className="electronic_sub_block_card_content_text">
                  <p>{item.product_name}</p>
                  <h4>{item.product_heading}</h4>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </>
  )
}

export default EssentialProduct
